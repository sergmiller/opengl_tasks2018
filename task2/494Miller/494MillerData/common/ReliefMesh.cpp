#include <Mesh.hpp>
#include <ReliefMesh.hpp>

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <iostream>
#include <vector>

#include <algorithm>
#include <random>

#include <PerlinNoise.hpp>



using std::pair;
using std::vector;

glm::vec3 get_normal(std::vector <glm::vec3> v) {
    glm::vec3 vec01 =  v[1] - v[0];
    glm::vec3 vec02 =  v[2] - v[0];
    glm::vec3 norm = normalize(cross(vec01, vec02));
    if(norm[2] < 0) {
        norm *= -1;
    }

    return norm;
}

void add_triangled_square(glm::vec4 h, glm::vec2 v,
                        std::vector<glm::vec3>& vertices,
                        std::vector<glm::vec3>& normals,
                        std::vector<glm::vec2>& texcoords, float scale) {
    float x = v[0];
    float y = v[1];

    glm::vec3 p = glm::vec3(x, y, h[0]) * scale;
    glm::vec3 p_x = glm::vec3(x + 1, y, h[1]) * scale;
    glm::vec3 p_y = glm::vec3(x, y + 1, h[2]) * scale;
    glm::vec3 p_xy = glm::vec3(x + 1, y + 1, h[3]) * scale;


    vertices.push_back(p);
    vertices.push_back(p_x);
    vertices.push_back(p_xy);

    glm::vec3 norm1 = get_normal(std::vector <glm::vec3>({p, p_x, p_xy}));

    normals.push_back(norm1);
    normals.push_back(norm1);
    normals.push_back(norm1);

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));
    texcoords.push_back(glm::vec2(1.0, 1.0));

    vertices.push_back(p);
    vertices.push_back(p_y);
    vertices.push_back(p_xy);

    glm::vec3 norm2 = get_normal(std::vector <glm::vec3>({p, p_y, p_xy}));

    normals.push_back(norm2);
    normals.push_back(norm2);
    normals.push_back(norm2);

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));
    texcoords.push_back(glm::vec2(1.0, 1.0));
}

pair<MeshPtr, vector <vector <float> > >  makeRelief(size_t Xtitles, size_t Ytitles, float height,
    float smooth, float scale, bool moreRandom)
{

    std::mt19937 gen(42);
    std::uniform_real_distribution<> dis(0, 1);


    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    PerlinNoise pn;

    std::vector <std::vector <float> > pert(Xtitles, std::vector <float>(Ytitles));
    std::vector <std::vector <float> > h(Xtitles, std::vector <float>(Ytitles));
    std::vector <std::vector <float> > _height(Xtitles, std::vector <float>(Ytitles));

    for(size_t x = 0; x < Xtitles; ++x) {
        for(size_t y = 0;y < Ytitles; ++y) {
            pert[x][y] = dis(gen);
            h[x][y] = pn.noise(float(x) / Xtitles * smooth, float(y) / Ytitles * smooth, 1);
        }
    }


    for(size_t x = 0; x < Xtitles - 1; ++x) {
        for(size_t y = 0;y < Ytitles - 1; ++y) {
            glm::vec4 heights = glm::vec4(h[x][y], h[x+1][y], h[x][y+1], h[x+1][y+1]);
            if(moreRandom) {
                heights += glm::vec4(pert[x][y],pert[x+1][y],pert[x][y+1],pert[x+1][y+1]) / smooth * 0.5f;

            }

            _height[x][y] = heights[0] * height;

            add_triangled_square(heights * height,
                glm::vec2(x, y), vertices, normals, texcoords, scale);
        }
    }

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Relief is created with " << vertices.size() << " vertices\n";

    return std::make_pair(mesh, _height);
}
