#version 330

uniform sampler2D mirrorTex;

in vec2 texCoord;

out vec4 fragColor; //выходной цвет фрагмента

void main()

{
    vec3 color = texture(mirrorTex, texCoord).rgb;
	fragColor = vec4(color, 1.0);
}
