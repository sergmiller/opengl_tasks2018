/**
Использование текстуры в качестве коэффициента отражения света
*/

#version 330

uniform sampler2D diffuseTex1;
uniform sampler2D diffuseTex2;
uniform sampler2D diffuseTex3;
uniform sampler2D diffuseTex4;


struct LightInfo
{
	vec3 pos; //положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
	vec3 La; //цвет и интенсивность окружающего света
	vec3 Ld; //цвет и интенсивность диффузного света
	vec3 Ls; //цвет и интенсивность бликового света
};
uniform LightInfo light;

in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec3 coords; // локальные координаты
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

const vec3 Ks = vec3(1.0, 1.0, 1.0); //Коэффициент бликового отражения
const float shininess = 128.0;



void main()
{
	vec4 mask = vec4(1 / (abs(0.8 - coords.z)),
					1 / (abs(1.3 - coords.z)),
					1 / (abs(1.6 - coords.z)),
					1 / (abs(2.1 - coords.z)));
	mask = normalize(mask);
	vec3 diffuseColor = mask.r * texture(diffuseTex1, texCoord).rgb +
						mask.g * texture(diffuseTex2, texCoord).rgb +
						mask.b * texture(diffuseTex3, texCoord).rgb +
						mask.a * texture(diffuseTex4, texCoord).rgb
						;

	vec3 normal = normalize(normalCamSpace); //нормализуем нормаль после интерполяции
	vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))

	vec3 lightDirCamSpace = normalize(light.pos - posCamSpace.xyz); //направление на источник света

	float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)

	vec3 color = diffuseColor * (light.La + light.Ld * NdotL);

	if (NdotL > 0.0)
	{
		vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света

		float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну
		blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика
		color += light.Ls * min(vec3(1.0), (Ks * (coords.z - 1.4) * (coords.z - 1.4) * 10)) * blinnTerm; // блик примитивным образом зависит от высоты
	}

	fragColor = vec4(color, 1.0);
}
