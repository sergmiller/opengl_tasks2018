#pragma once

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <GL/glew.h>

#include <string>
#include <map>
#include <memory>

#include <vector>

using std::pair;
using std::vector;
/**
Создает рельеф по сетке размером Xtitles * Ytitles методом шума Перлина
с высотами от 0 до height,
*/
pair<MeshPtr, vector <vector <float> > > makeRelief(size_t Xtitles=100, size_t Ytitles=100,
    float height=1, float smooth=1, float scale=1, bool moreRandom=true, bool goodBound=true);
