#include <Application.hpp>
#include <LightInfo.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>
#include <Framebuffer.hpp>

#include <iostream>
#include <vector>

#include <ReliefMesh.hpp>

const std::string DATA_PATH = "494MillerData/";
const float INF = 1e20;

class ReliefApplication : public Application
{
public:
    MeshPtr _water;
    MeshPtr _cube;
    MeshPtr _bunny;
    MeshPtr _teapot;
    MeshPtr _relief;
    MeshPtr _mirror;

    glm::vec3 _lightPosCamSpace;

    float height = 30.0;
    float bottom = -3;
    float scale = 0.1;
    float smooth = 10;
    size_t titles = 300;

    float waterTransparency = 0.1;
    float waterLevel = -2.;

    MeshPtr _marker; //Маркер для источника света
    MeshPtr _sky; // небо


    //Координаты источника света
    float _lr = 5.1e1;
    float _phi = 0.0;
    float _theta = glm::pi<float>() * 0.25f;


    float _mirror_radius = -3.0f;
    float _mirror_alpha = 0;
    float _mirror_beta = 0;
    float _mirror_gamma = 0;
    float _mirror_theta = 0;
    float _mirror_phi = -glm::pi<float>() * 0.5f;


    vector <vector < float> > _heights;

    // ShaderProgramPtr _shader;
    // ShaderProgramPtr _markerShader;

    //Идентификатор шейдерной программы
    ShaderProgramPtr _shaderTemp;
    ShaderProgramPtr _shaderNorm;
    ShaderProgramPtr _shaderWhite;

    ShaderProgramPtr _mirrorShader;
    ShaderProgramPtr _skyShader;
    ShaderProgramPtr _waterShader;
    ShaderProgramPtr _lightShader;
    ShaderProgramPtr _groundShader;
    ShaderProgramPtr _markerShader;

    TexturePtr _texture[6];
    TexturePtr _mirrorTexture;

    LightInfo _light;

    GLuint _sampler;
    GLuint _waterSampler;
    GLuint _framebuffer;
    GLuint _depthbuffer;

    glm::vec3 _relief_pos;

    float _exposure = 1.0f; //Параметр алгоритма ToneMapping
	float _brightnessThreshold = 1.0f; // Порог яркости для применения размытия.
	float _bloomFactor = 5.0f; // Фактор учёта заблуренной яркости.


    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        // _mirror = makeMirror();

        _water = makeGroundPlane(100, 1);
        _water->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, waterLevel)));

        _cube = makeCube(0.5f);
        _cube->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));


        auto reliefFata = makeRelief(titles, titles, height, smooth, scale);
        _relief = reliefFata.first;
        _relief_pos = glm::vec3(-float(titles) * scale / 2, -float(titles) * scale / 2, bottom);

        _relief->setModelMatrix(glm::translate(glm::mat4(1.0f), _relief_pos));

        _heights = reliefFata.second;

        //Создаем меш из файла
        _bunny = loadFromFile(DATA_PATH + "models/bunny.obj");
        _bunny->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, 0.0f)));

        _teapot = loadFromFile(DATA_PATH + "models/teapot.obj");
        _teapot->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(1.0f, 3.0f, 0.0f)));

        _marker = makeSphere(0.1f);

        _sky = makeSphere(5e1);
        _sky->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));


        //Создаем шейдерную программу
        _mirrorShader = std::make_shared<ShaderProgram>(DATA_PATH + "shaders/water.vert",
                    DATA_PATH + "shaders/water.frag");

        _waterShader = std::make_shared<ShaderProgram>(DATA_PATH + "shaders/water.vert",
                    DATA_PATH + "shaders/water.frag");

        _lightShader = std::make_shared<ShaderProgram>(DATA_PATH + "shaders/diffusePlusGradientPointLight.vert",
                    DATA_PATH + "shaders/diffusePointLight.frag");

        _skyShader = std::make_shared<ShaderProgram>(DATA_PATH + "shaders/texture.vert",
                    DATA_PATH + "shaders/texture.frag");

        _groundShader = std::make_shared<ShaderProgram>(DATA_PATH + "shaders/ground.vert",
                    DATA_PATH + "shaders/ground.frag");

        _markerShader = std::make_shared<ShaderProgram>(DATA_PATH + "shaders/marker.vert",
                    DATA_PATH + "shaders/marker.frag");

        _shaderNorm = std::make_shared<ShaderProgram>(DATA_PATH + "shaders/shaderNorm.vert",
                    DATA_PATH + "shaders/shader.frag");

        _shaderTemp = std::make_shared<ShaderProgram>(DATA_PATH + "shaders/shaderTemp.vert",
                    DATA_PATH + "shaders/shader.frag");

        _shaderWhite = std::make_shared<ShaderProgram>(DATA_PATH + "shaders/shaderWhite.vert",
                    DATA_PATH + "shaders/shader.frag");


        //=========================================================
        //Инициализация значений переменных освщения
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta),
                                    glm::sin(_phi) * glm::cos(_theta),
                                    glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);
        //=========================================================

        // 4 типа поверхности
        _texture[0] = loadTexture(DATA_PATH + "textures/sand.jpg");
        _texture[1] = loadTexture(DATA_PATH + "textures/grass.jpg");
        _texture[2] = loadTexture(DATA_PATH + "textures/ground.jpg");
        _texture[3] = loadTexture(DATA_PATH + "textures/snow.jpg");

        // небо
        _texture[4] = loadTexture(DATA_PATH + "textures/sky.jpg");

        // вода
        _texture[5] = loadTexture(DATA_PATH + "textures/water2.jpg");

        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

        // glGenSamplers(1, &_waterSampler);
        // glSamplerParameteri(_waterSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        // glSamplerParameteri(_waterSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);


        // glGenFramebuffers(1, &_framebuffer);
        // glBindFramebuffer(GL_FRAMEBUFFER, _framebuffer);
        // glViewport(0, 0, 1024, 1024);
        // glGenRenderbuffers(1, &_depthbuffer);
        // glBindRenderbuffer(GL_RENDERBUFFER, _depthbuffer);
        // glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, 1024, 1024);
        // glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, _depthbuffer);


        _mirrorTexture = std::make_shared<Texture>(GL_TEXTURE_2D);
        _mirrorTexture->setTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 1024, 1024, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
        _mirrorTexture->attachToFramebuffer(GL_COLOR_ATTACHMENT0);
    }

    void update() override {
        double dt = glfwGetTime() - _oldTime;
        _oldTime = glfwGetTime();

        _cameraMover->update(_window, dt);
        clamp_camera_position(std::dynamic_pointer_cast<FreeCameraMover>(_cameraMover)->_pos);
        _cameraMover->updateView(_window);
        _camera = _cameraMover->cameraInfo();
    }

    void clamp_camera_position(glm::vec3 & pos) {
        float z = clamp_z_with_heights(pos);
        float eps = 0.2;
        if(pos[2] < z + eps) {
            pos[2] = z + eps;
        }
    }

    float clamp_z_with_heights(const glm::vec3 & pos) const {
        double x_pos = pos[0] -  _relief_pos[0];
        double y_pos = pos[1] -  _relief_pos[1];
        if(x_pos < 0.0 || x_pos >= titles * scale) return -INF;
        if(y_pos < 0.0 || y_pos >= titles * scale) return -INF;

        float xrate = ((x_pos/scale)/titles) * _heights.size();
        float yrate = ((y_pos/scale)/titles) * _heights[0].size();

        int ix = std::min(int(xrate), (int)_heights.size() - 1);
        int iy = std::min(int(yrate), (int)_heights[0].size() - 1);
        float tx = xrate - int(xrate);
        float ty = yrate - int(yrate);


        int ixr = std::min(ix + 1, (int)_heights.size() - 1);
        int iyr = std::min(iy + 1, (int)_heights[0].size() - 1);

        float h1 = _heights[ix][iy];
        float h2 = _heights[ixr][iy];
        float h3 = _heights[ixr][iyr];
        float h4 = _heights[ix][iyr];

        float h = (h1 * (1.0 - ty) + h4 * ty) * (1.0 - tx) + (h2 * (1.0 - ty) + h3 * ty) * tx;

        return h * scale + _relief_pos[2];
    }

    void updateGUI() override {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Task3", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

                ImGui::SliderFloat("radius", &_lr, 0.1f, 1e2);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
            }
        }
        ImGui::End();
    }


    void drawWithShader(ShaderProgramPtr& shader, MeshPtr& mesh, bool drawFrame=false,
        glm::vec3 specular=glm::vec3(0, 0, 0)) {


        shader->setVec3Uniform("material.Ka", _light.ambient);
        shader->setVec3Uniform("material.Kd", _light.diffuse);
        shader->setVec3Uniform("material.Kg", _light.specular);

        shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        shader->setMat4Uniform("modelMatrix", mesh->modelMatrix());

        shader->setMat3Uniform("normalToCameraMatrix",
                        glm::transpose(glm::inverse(
                        glm::mat3(_camera.viewMatrix * mesh->modelMatrix()))));

        if (drawFrame) {
            glEnable(GL_POLYGON_OFFSET_LINE);
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            glPolygonOffset(0, -10000);
        }

        mesh->draw();

        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glDisable(GL_POLYGON_OFFSET_LINE);
    }

    void initShader(ShaderProgramPtr& shader) {
        shader->setVec3Uniform("light.pos", _lightPosCamSpace);
        shader->setVec3Uniform("light.La", _light.ambient);
        shader->setVec3Uniform("light.Ld", _light.diffuse);
        shader->setVec3Uniform("light.Ls", _light.specular);
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta),
                                    glm::sin(_phi) * glm::cos(_theta),
                                    glm::sin(_theta)) * _lr;

        _lightPosCamSpace = glm::vec3(_camera.viewMatrix
                                        * glm::vec4(_light.position, 1.0));

        {
            _lightShader->use();

            initShader(_lightShader);

            drawWithShader(_lightShader, _bunny);

            drawWithShader(_lightShader, _teapot);

            // drawWithShader(_lightShader, _cube);
        }

        {
            _groundShader->use();

            initShader(_groundShader);

            GLuint textureUnitForDiffuseTex[4] = {0, 0, 0, 0};

            if (USE_DSA) {
                GLuint samplers[4] = { _sampler, _sampler, _sampler, _sampler};
                glBindSamplers(0, 4, samplers);

                GLuint textures[4];
                for(size_t i = 0;i < 4; ++i) {
                    textures[i] = _texture[i]->texture();
                }


                glBindTextures(0, 4, textures);

                _groundShader->setIntUniform("diffuseTex1", 0);
                _groundShader->setIntUniform("diffuseTex2", 1);
                _groundShader->setIntUniform("diffuseTex3", 2);
                _groundShader->setIntUniform("diffuseTex4", 3);

            } else {
                for(size_t i = 0;i < 4; ++i) {
                    glActiveTexture(GL_TEXTURE0 + i);  //текстурный юнит i
                    glBindSampler(i, _sampler);
                    _texture[i]->bind();
                    _groundShader->setIntUniform("diffuseTex" + std::to_string(i + 1), i);
                }
            }


            drawWithShader(_groundShader, _relief);

        }

        glm::mat4 mirrorModelMatrix = getMirrorModelMatrix();
		glm::mat4 mirrorViewMatrix = getMirrorViewMatrix(mirrorModelMatrix);
		glm::mat4 mirrorProjectionMatrix = getMirrorProjectionMatrix(mirrorModelMatrix, mirrorViewMatrix);


        // Прозрачная вода
        {
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

            _waterShader->use();

            initShader(_waterShader);
            _waterShader->setVec3Uniform("light.dir", _light.position / _lr);

            glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
            glBindSampler(0, _waterSampler);
            _texture[5]->bind();
            _waterShader->setIntUniform("waterTex", 0);

            _waterShader->setFloatUniform("transparency", waterTransparency);

            drawWithShader(_waterShader, _water);

            glDisable(GL_BLEND);
        }


        // Рисуем небо
        {
            _skyShader->use();

            // // Отражение неба
            // glBindFramebuffer(GL_FRAMEBUFFER, _framebuffer);
            // glViewport(0, 0, 1024, 1024);
            // glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
            // // glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            // //
            // _skyShader->setVec3Uniform("pointLight.pos",
            //     glm::vec3(mirrorViewMatrix * glm::vec4(_light.position, 1.0)));
            // _skyShader->setMat3Uniform("normalToCameraMatrix",
            //     glm::transpose(glm::inverse(glm::mat3(mirrorViewMatrix * _sky->modelMatrix()))));
            // _skyShader->setMat4Uniform("viewMatrix", mirrorViewMatrix);
            // _skyShader->setMat4Uniform("projectionMatrix", mirrorProjectionMatrix);
            //
            // glActiveTexture(GL_TEXTURE0);  //текстурный юнит i
            // glBindSampler(0, _sampler);
            // _texture[4]->bind();
            // _skyShader->setIntUniform("diffuseTex", 0);
            //
            // _sky->draw();
            //

            // ****


            glBindFramebuffer(GL_FRAMEBUFFER, 0);
            glViewport(0, 0, width, height);
            glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
            // glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


            initShader(_skyShader);

            glActiveTexture(GL_TEXTURE0);  //текстурный юнит i
            glBindSampler(0, _sampler);
            _texture[4]->bind();
            _skyShader->setIntUniform("diffuseTex", 0);

            drawWithShader(_skyShader, _sky);



        }

        //Рисуем маркер для источника света
        {
            _markerShader->use();
            _markerShader->setMat4Uniform("mvpMatrix",
                _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
            _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
            _marker->draw();
        }

        {
			_mirrorShader->use();
			_mirrorShader->setMat4Uniform("mvpMatrix",
                _camera.projMatrix * _camera.viewMatrix * mirrorModelMatrix);
			glActiveTexture(GL_TEXTURE0);
		    _texture[5]->bind();
			_mirrorShader->setIntUniform("mirrorTex", 0);
			_water->draw();
			_mirrorTexture->unbind();
		}

        // _shaderWhite->use();
        // _shaderWhite->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        // _shaderWhite->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        //
        // _shaderWhite->setMat4Uniform("modelMatrix", _relief->modelMatrix());
        //
        // glEnable(GL_POLYGON_OFFSET_LINE);
        // glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        // glPolygonOffset(0, -10000);
        // _relief->draw();
        // glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        // glDisable(GL_POLYGON_OFFSET_LINE);
        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }

    glm::mat4 getMirrorModelMatrix() {
        glm::mat4 alphaRotateMatrix = glm::rotate(glm::mat4(1.0f), _mirror_alpha, glm::vec3(0, 0, 1));
        glm::mat4 betaRotateMatrix =  glm::rotate(glm::mat4(1.0f), _mirror_beta, glm::vec3(0, 1, 0));
        glm::mat4 gammaRotateMatrix = glm::rotate(glm::mat4(1.0f), _mirror_gamma, glm::vec3(1, 0, 0));

        glm::vec3 pos = glm::vec3(
        glm::cos(_mirror_phi) * glm::cos(_mirror_theta),
        glm::sin(_mirror_phi) * glm::cos(_mirror_theta),
        glm::sin(_mirror_theta)) * _mirror_radius;

        glm::mat4 translate = glm::translate(glm::mat4(1.0f), pos);
        glm::mat4 mirrorModelMatrix = translate * gammaRotateMatrix * betaRotateMatrix * alphaRotateMatrix;

        return mirrorModelMatrix;
    }

    glm::mat4 getMirrorViewMatrix(glm::mat4& mirrorModelMatrix) {
         glm::vec3 viewPosition(glm::inverse(_camera.viewMatrix) * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
         glm::vec3 point(mirrorModelMatrix * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
         glm::vec3 normal(mirrorModelMatrix * glm::vec4(1.0f, 0.0f, 0.0f, 0.0f));
         glm::vec3 eyePosition = viewPosition + 2.0f * normal * glm::dot(point - viewPosition, normal);
         glm::vec3 center = (viewPosition + eyePosition) / 2.0f;
         glm::vec3 up(mirrorModelMatrix * glm::vec4(0.0f, 0.0f, 1.0f, 0.0f));

         glm::mat4 mirrorViewMatrix = glm::lookAt(eyePosition, center, up);

         return mirrorViewMatrix;
     }

     glm::mat4 getMirrorProjectionMatrix(glm::mat4& mirrorModelMatrix, glm::mat4& mirrorViewMatrix) {
         glm::mat4 mv = mirrorViewMatrix * mirrorModelMatrix;
         glm::vec3 bottomLeft = glm::vec3(mv * glm::vec4(0.0f, -1.0f, -1.0f, 1.0f));
         glm::vec3 topRight = glm::vec3(mv * glm::vec4(0.0f, 1.0f, 1.0f, 1.0f));

         glm::mat4 mirrorProjectionMatrix = glm::frustum(bottomLeft.x, topRight.x, bottomLeft.y, topRight.y, glm::abs(bottomLeft.z), 100.0f);

         return mirrorProjectionMatrix;
     }
};

int main()
{
    ReliefApplication app;
    app.start();

    return 0;
}
