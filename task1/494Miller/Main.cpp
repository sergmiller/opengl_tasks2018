#include <Application.hpp>
#include <LightInfo.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <vector>

#include <ReliefMesh.hpp>

const std::string DATA_PATH = "494MillerData/";


class ReliefApplication : public Application
{
public:
    MeshPtr _cube;
    MeshPtr _bunny;
    MeshPtr _teapot;
    MeshPtr _relief;

    float height = 30.0;
    float bottom = -3;
    float scale = 0.1;
    float smooth = 10;
    size_t titles = 200;

    //Идентификатор шейдерной программы
    ShaderProgramPtr _shaderTemp;
    ShaderProgramPtr _shaderNorm;
    ShaderProgramPtr _shaderWhite;

    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        //Создаем меш с кубом
        _cube = makeCube(0.5f);
        _cube->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));


        _relief = makeRelief(titles, titles, height, smooth, scale, true);
        _relief->setModelMatrix(glm::translate(glm::mat4(1.0f),
                    glm::vec3(-float(titles) * scale / 2, -float(titles) * scale / 2, bottom)));

        //Создаем меш из файла
        _bunny = loadFromFile(DATA_PATH + "models/bunny.obj");
        _bunny->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, 0.0f)));

        _teapot = loadFromFile(DATA_PATH + "models/teapot.obj");
        _teapot->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(1.0f, 3.0f, 0.0f)));

        //Создаем шейдерную программу
        _shaderNorm = std::make_shared<ShaderProgram>(DATA_PATH + "shaders/shaderNorm.vert",
                    DATA_PATH + "shaders/shader.frag");

        _shaderTemp = std::make_shared<ShaderProgram>(DATA_PATH + "shaders/shaderTemp.vert",
                    DATA_PATH + "shaders/shader.frag");

        _shaderWhite = std::make_shared<ShaderProgram>(DATA_PATH + "shaders/shaderWhite.vert",
                    DATA_PATH + "shaders/shader.frag");
    }


    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Устанавливаем шейдер.
        _shaderNorm->use();
        _shaderNorm->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shaderNorm->setMat4Uniform("projectionMatrix", _camera.projMatrix);


        _shaderNorm->setMat4Uniform("modelMatrix", _cube->modelMatrix());
        _cube->draw();

        _shaderNorm->setMat4Uniform("modelMatrix", _bunny->modelMatrix());
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        _bunny->draw();
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        _shaderNorm->setMat4Uniform("modelMatrix", _teapot->modelMatrix());
        _teapot->draw();

        _shaderTemp->use();
        _shaderTemp->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shaderTemp->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _shaderTemp->setMat4Uniform("modelMatrix", _relief->modelMatrix());
        _relief->draw();

        _shaderWhite->use();
        _shaderWhite->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shaderWhite->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _shaderWhite->setMat4Uniform("modelMatrix", _relief->modelMatrix());
        glEnable(GL_POLYGON_OFFSET_LINE);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glPolygonOffset(0, -10000);
        _relief->draw();
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glDisable(GL_POLYGON_OFFSET_LINE);
    }
};

int main()
{
    ReliefApplication app;
    app.start();

    return 0;
}
